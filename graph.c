//
// Created by C19Trevor.Westendorf and Leo Tanja on 4/27/2017.
//
#include "Graph.h"

Graph * graphCreate(int numberVertices, int bytesPerNode){
    Graph *graph = (Graph *) malloc( sizeof(Graph));

    graph->numberVertices = numberVertices;
    graph->vertices = (void *) malloc( (size_t)numberVertices * bytesPerNode);
    graph->edges = (int **) malloc( numberVertices * sizeof(int *));

    // Create each row of the adjacency matrix
    for (int j=0; j<numberVertices; j++) {
        graph->edges[j] = (int *) malloc( numberVertices * sizeof(int));
    }

    // Set every edge to FALSE
    for (int row = 0; row < graph->numberVertices; row++) {
        for (int col = 0; col < graph->numberVertices; col++) {
            graph->edges[row][col] = 0;
        }
    }
    return graph;
}

void    graphDelete(Graph *graph){
    for (int row = 0; row < graph->numberVertices; row++) {
        for (int col = 0; col < graph->numberVertices; col++) {
            free(&graph->edges[row][col]);
        }
        free(&graph->vertices[row]);
    }
    free(graph);
}

void    graphSetEdge(Graph *graph, int fromVertex, int toVertex, int state){
    if (fromVertex >= 0 && fromVertex < graph->numberVertices &&
        toVertex   >= 0 && toVertex   < graph->numberVertices) {
        graph->edges[fromVertex][toVertex] = state;
//        graph->visited[fromVertex][toVertex] = WHITE;
    }
}

int     graphGetEdge(Graph *graph, int fromVertex, int toVertex){
    return graph->edges[fromVertex][toVertex];
}



