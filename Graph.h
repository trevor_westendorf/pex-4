/** Graph.h
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Modified by: Leo Tanja and Trevor Westendorf
 * Section: M3
 * Project: PEX4
 * Purpose: The definition of a graph.
 * ===========================================================
 */

#ifndef GRAPH_H
#define GRAPH_H

#define BLACK 0
#define GREY  1
#define WHITE 2

#include<stdlib.h>

typedef struct graph{
	int    numberVertices;
	void * vertices;  // Array of nodes
	int ** edges;     // Adjacency matrix (2D Array)
	int *  discover;
	int * visited; // Used for traversing and isolating a winning path
} Graph;

Graph * graphCreate(int numberVertices, int bytesPerNode);
void    graphDelete(Graph *graph);
void    graphSetEdge(Graph *graph, int fromVertex, int toVertex, int state);
int     graphGetEdge(Graph *graph, int fromVertex, int toVertex);


#endif // GRAPH_H